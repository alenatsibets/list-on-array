import java.util.Arrays;

public class List {
    private int[] p;
    private int count;
    public List() {
        count = 0;
    }
    public void add(int item) {
        int[] p2;
        p2 = p;
        p = new int[count + 1];
        if (count > 0) {System.arraycopy(p2, 0, p, 0, count);}
        p[count] = item;
        count++;
    }
    public void insert(int item, int index) {
        if (index < 0 || index > count) {
            throw new RuntimeException("Queue is clear.");
        }
        if (index == count) {
            add(item);
            return;
        }
        int[] p2;
        p2 = p;
        p = new int[count + 1];
        System.arraycopy(p2, 0, p, 0, index);
        if (count + 1 - (index + 1) >= 0) {System.arraycopy(p2, index + 1 - 1, p, index + 1, count + 1 - (index + 1));}
        p[index] = item;
        count++;
    }
    public void delete(int index) {
        if (index < 0 || index > count) {
            throw new RuntimeException("Queue is clear.");
        }
        if (index == 0) {
            pop();
            return;
        }
        int[] p2;
        p2 = p;
        p = new int[count - 1];
        System.arraycopy(p2, 0, p, 0, index);
        if (count - 1 - index >= 0) {System.arraycopy(p2, index + 1, p, index, count - 1 - index);}
        count--;
    }
    public void replace(int item, int index) {
        if (index < 0 || index > count) {
            throw new RuntimeException("Queue is clear.");
        }
        p[index] = item;
    }

    public int get(int index) {
        if (index < 0 || index > count - 1) {
            throw new RuntimeException("Index is invalid.");
        }
        return p[index];
    }
    public int pop() {
        if (count == 0) {
            throw new RuntimeException("Index is invalid.");
        }
        int item;
        item = p[0];
        int[] p2;
        p2 = new int[count - 1];
        count--;
        System.arraycopy(p, 1, p2, 0, count);
        p = p2;
        return item;
    }
    public int getFirst() {
        if (count == 0) {
            throw new RuntimeException("Queue is clear.");
        }
        return p[0];
    }
    public void clear() {
        p = null;
        count = 0;
    }
    public boolean isEmpty() {
        return count == 0;
    }
    public int size() {
        return count;
    }

    @Override
    public String toString() {
        return "List{" +
                "p=" + Arrays.toString(p) +
                ", count=" + count +
                '}';
    }
}
