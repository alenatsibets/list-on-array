import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        List list = new List();
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        System.out.println(list.isEmpty());
        System.out.println(list);
        list.clear();
        System.out.println(list.isEmpty());
        System.out.println(list);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        System.out.println(list);
        System.out.println("size = " + list.size());
        System.out.println("First element: " + list.getFirst());
        System.out.println("Enter a index: ");
        Scanner sc = new Scanner(System.in);
        int index = sc.nextInt();
        System.out.println("Element on " + index + " position: " + list.get(index));
        list.delete(index);
        System.out.println(list);
        System.out.println(list.pop());
        System.out.println(list);
        list.insert(5, index);
        System.out.println(list);
        list.replace(99, index);
        System.out.println(list);

    }
}